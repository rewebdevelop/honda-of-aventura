  var destiny = {
      lat: 25.925567,
      lng: -80.157123
  };
var directionsService = new google.maps.DirectionsService();

  var map = new google.maps.Map(document.getElementById('mapa'), {
      center: destiny,
      scrollwheel: false,
      zoom: 7
  });

  var directionsDisplay = new google.maps.DirectionsRenderer({
      map: map
  });

$("#direction_form").submit(function(event) {
	event.preventDefault();

	var meu_local = $("#txtEnderecoPartida").val();

	var request = {
		origin: meu_local,
		destination: destiny,
		travelMode: google.maps.TravelMode.DRIVING
	};

	directionsService.route(request, function(result, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(result);
		}
	});
});
