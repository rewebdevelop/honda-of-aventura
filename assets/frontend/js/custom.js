$('.panel-heading').on('click', function (e) {
    if ($(this).parents('.panel').children('.panel-collapse').hasClass('in')) {
        e.stopPropagation();
    }
});

$('#homeCarousel').carousel({
    swipe: 30
});
$('#homeCarousel2').carousel({
    swipe: 30
});
$('.slider-carros-home').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
        {
            breakpoint: 991,
            settings: {
                slidesToShow: 3
            }
            },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2
            }
            },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1
            }
            }
        ]


});

$(document).ready(function () {
    $('#map').addClass('scrolloff'); // set the mouse events to none when doc is ready

    $('#overlay').on("mouseup", function () { // lock it when mouse up
        $('#map').addClass('scrolloff');
        //somehow the mouseup event doesn't get call...
    });
    $('#overlay').on("mousedown", function () { // when mouse down, set the mouse events free
        $('#map').removeClass('scrolloff');
    });
    $("#map").mouseleave(function () { // becuase the mouse up doesn't work...
        $('#map').addClass('scrolloff'); // set the pointer events to none when mouse leaves the map area
        // or you can do it on some other event
    });



});

$(window).load(function () {
    // The slider being synced must be initialized first
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        arrows: false,
        fade: true,
        initialSlide: 2,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        infinite: false,
        centerMode: true,
        centerPadding: '60px',
        focusOnSelect: true,
        initialSlide: 2,
    });
    $('.used-car-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        arrows: false,
        fade: true,
        initialSlide: 1,
        asNavFor: '.used-car-carousel'
    });
    $('.used-car-carousel').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.used-car-slider',
        infinite: false,
        centerMode: true,
        centerPadding: '60px',
        initialSlide: 1,
        focusOnSelect: true,
        responsive: [
            {
              breakpoint: 991,
              settings: {
                  slidesToShow: 1,
                  centerMode: false
              }
            }
        ]
    });
});

$('.stack-table').stacktable({
    myClass: 'stacktable small-only'
});

$(function () {
    $(".datepicker").datepicker({
        minDate: 0
    });
});

$('input[name=ano]').inputmask('9999/9999');
$('[name=phone]').inputmask('999 9999 9999');
$('[name=email]').inputmask({
    alias: "email"
});
$('[cel]').inputmask('999 9999 9999');
$('[name=cellphone]').inputmask('999 9999 9999');
$('[name=fecha]').inputmask('99/99/9999');
$('[name=data_nasc]').inputmask('99/99/9999');
$('[name=placa]').inputmask('aaa 9999');
$('.number').inputmask('9999999999');

$(document).on("click", ".promo-modal", function () {
    var carro = $(this).data('carro');
    $(".modal-body #carro").val(carro);
    $("#carro").prop('disabled', 'disabled');
    /* setting input box value to selected option value */
});
$(document).on("click", ".promo-modal2", function () {
    var carro = $(this).data('carro');
    $(".modal-body #carro2").val(carro);
    $("#carro2").prop('disabled', 'disabled');
    /* setting input box value to selected option value */
});

$(".close-form").click(function () {
    $("#modal_seminovos").hide();
});

if ($(window).width() >= 768) {

    $('.navbar .dropdown').hover(function () {
        $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();
    }, function () {
        $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();
    });

} else {

    $('.navbar .dropdown').click(function () {
        e.preventDefault();

        $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideToggle();

    })
}

$(window).resize(function () {

    if ($(window).width() >= 768) {

        $('.navbar .dropdown').hover(function () {
            $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();
        }, function () {
            $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp()
        });

    }

});

//Modal Solicitar Preço Especial
$('.enviar-celular').click(function (e) {
    e.preventDefault();

    var carId = $(this).attr('id');
    var carUrl = $(this).siblings('a').attr('href');
    var carUnidade = $(this).attr('data-unidade');

    $('#enviar-celular').attr('name', 'enviar-celular-' + carId);
    $('#enviar-celular').find('#enviar-celular-carro').val(carUrl);
    $('#enviar-celular').find('#unidade').val(carUnidade);

    $('#enviar-celular').modal('show');

});
/* ----------- Tab Jquery ----------- */
$(".tab-content").hide(); //Hide all content
$(".tabnav li:first").addClass("active").show(); //Activate first tab
$(".tab-content:first").show(); //Show first tab content
//On Click Event

//Promoções
$('.new-car-parts').click(function () {
    var modelo = $(this).data('model');
    $('input[name=model]').val(modelo);
});
$('.new-car-parts').click(function () {
    var modelo = $(this).data('model');
    $('.new-car-parts').removeClass('selected');
    $(this).addClass('selected');
    $('input[name=model]').val(modelo);
    var aTag = $(".form-bottom");
    $('html,body').animate({
        scrollTop: aTag.offset().top
    }, 'slow');

});
