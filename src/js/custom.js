$('.panel-heading h4').on('click', function (e) {
    if ($(this).parents('.panel').children('.panel-collapse').hasClass('in')) {
        e.stopPropagation();
    }
});

$('.slider-carros-home').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1
});

$(window).load(function () {
    // The slider being synced must be initialized first
    $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 210,
        itemHeight: 150,
        itemMargin: 5,
        asNavFor: '#slider'
    });

    $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
    });
});

$('#table1').stacktable({
    myClass: 'stacktable small-only'
});

$('#table2').stacktable({
    myClass: 'stacktable small-only'
});

$('#table3').stacktable({
    myClass: 'stacktable small-only'
});

$('#table4').stacktable({
    myClass: 'stacktable small-only'
});

$('#table5').stacktable({
    myClass: 'stacktable small-only'
});

$('#table6').stacktable({
    myClass: 'stacktable small-only'
});
