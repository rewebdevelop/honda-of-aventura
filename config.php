<?php
define('THEME_NAME', "Honda-of-Aventura");
define('CLIENT_KEY', "e074a7e9939aad7747438ff6a55cb511");
define('SECRET_KEY', "z3dpoByS063HtXAIK6UqXGBDqycEj7Ps");
define('INTEGRATOR_KEY', "rewebtoken");
define('SMS_KEY', '6c3fe78695d79c5c317baf71b145e9f0');
//define('ROOT_URL', "http://localhost/honda-of-aventura/");
define('ROOT_URL', "http://interface.reweb.com.br/html/HondaofAventura/preview/");
define('DOCUMENT_ROOT', __DIR__);
define('CACHE_HOURS', 24);
define('EDIT_MODE', false);
define('CONTACT_EMAIL', "");
define('DEBUG_MODE', false);
define('URL_COMPLEMENT', "in-miami-hollywood-hialeah-davie-aventura");
define('URL_NEW_CARS', "autos-nuevos");
define('URL_USED_CARS', "autos-seminuevos");
define('URL_USED_CARS_DETAIL', "seminuevo");
define('URL_SERVED_AREAS', 'served-areas');
define('URL_SERVED_AREAS_DETAIL', 'served-area');

$apiBase = "http://dealers.rewebmkt.com/api/";
$crmBase = "http://crm.reweb.com.br/api/websites/";

$ENDPOINTS = array(
    'banners' => $crmBase . 'banners/',
    'highlights' => $crmBase . 'highlights/',
    'cities' => $crmBase . 'cities/',
    'stores' => $crmBase . 'stores/',
    'news' => $crmBase . 'news/',
    'blog_car' => $crmBase . 'news_car/',
    'promotions' => $crmBase . 'promotions/',
    'register_coment' => $crmBase .'register_coment/',
    'categories' => $crmBase . 'news_categories/',
    'category_news' => $crmBase . 'category_news/',
    'new_cars' => $crmBase . 'cars/',
    'translations' => $apiBase . 'client/site/languages',
    'forms' => $apiBase . 'form/',
    'menu' => $apiBase . 'client/site/menu',
    'page' => $apiBase . 'client/site/page/',
    'car_categories' => $apiBase . 'client/cars/categories',
    'car_detail' => $crmBase . 'cars/',
    'files' => $apiBase . 'client/site/files',
    'images' => $apiBase . 'client/site/images',
    'saveContentText' => $apiBase . 'client/site/saveTranslationText',
    'pages' => $apiBase . 'client/site/pages',
    'savePage' => $apiBase . 'client/site/savePage',
    'consortiums' => $crmBase . 'consortiums/',
    'products' => $crmBase . 'products/',
    'products_categories' => $crmBase . 'products_categories/',
    'products_search' => $crmBase . 'products_search/',
    'carPromotions' => $crmBase . 'promotions_cars/car-news/'
);
